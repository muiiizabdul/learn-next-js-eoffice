import Link from "next/link";
import { Squares2X2Icon, ArchiveBoxIcon } from '@heroicons/react/24/outline';

export default function Layout() {
    return (
        <div className="h-screen bg-indigo-300 flex flex-row">
            <div className="w-[120px] bg-primary h-full">
                <div className="flex flex-col items-center py-3">
                    {/* <div className="w-[70px] h-[50px] bg-white rounded-lg"></div> */}
                    <ul className="mt-8">
                        <li className="my-2">
                            <Link href="#">
                                <div className="w-[60px] h-[60px] rounded-lg flex items-center justify-center text-slate-200 hover:bg-slate-200 hover:text-primary">
                                    <Squares2X2Icon width={30} />
                                </div>
                            </Link>
                        </li>
                        <li className="my-2">
                            <Link href="#">
                                <div className="w-[60px] h-[60px] rounded-lg flex items-center justify-center text-slate-200 hover:bg-slate-200 hover:text-primary">
                                    <ArchiveBoxIcon width={30} />
                                </div>
                            </Link>
                        </li>
                        <li className="my-2">
                            <Link href="#">
                                <div className="w-[60px] h-[60px] rounded-lg flex items-center justify-center text-slate-200 hover:bg-slate-200 hover:text-primary">
                                    <ArchiveBoxIcon width={30} />
                                </div>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="">right</div>
        </div>
    );
}
