import Link from 'next/link';

export default function Page() {
    return (
        <>
            <h1 className="text-slate-700 text-[22px] font-medium">Forgot Password</h1>
            <p className="text-black my-2 text-sm font-light text-center">Enter your email and we'll send you instructions<br />to reset your password.</p>
            <input placeholder="Enter your email" type="email" className="mt-2 w-full border py-2 px-2.5 rounded font-light text-sm" />
            <button className="w-full bg-primary py-3 text-white rounded font-medium text-sm mt-3.5">Send</button>
            <Link href="/auth/signin" className="w-full py-3 text-white rounded font-medium text-sm text-primary mt-3.5 mb-3.5 text-center">Back to Sign In</Link>
        </>
    );
}
