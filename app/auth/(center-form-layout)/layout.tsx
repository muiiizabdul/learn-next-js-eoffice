import Link from 'next/link';
import React from 'react';

export default function Layout({ children }: { children: React.ReactNode }) {
    return (
        <div className="h-screen bg-primary flex items-center justify-center">
            <div className="bg-white w-[500px] rounded-lg flex flex-col items-center py-4 px-8">
                {children}
            </div>
        </div>
    );
}
