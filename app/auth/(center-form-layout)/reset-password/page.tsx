import Link from 'next/link';

export default function Page() {
    return (
        <>
            <h1 className="text-slate-700 text-[22px] font-medium">Reset Password</h1>
            <input placeholder="Enter your new password" type="password" className="mt-2 w-full border py-2 px-2.5 rounded font-light text-sm text-black" />
            <input placeholder="Confirm your new password" type="password" className="mt-3 w-full border py-2 px-2.5 rounded font-light text-sm text-black" />
            <button className="w-full bg-primary py-3 text-white rounded font-medium text-sm mt-3.5">Reset my password</button>
            <Link href="/auth/signin" className="w-full py-3 text-white rounded font-medium text-sm text-primary mt-3.5 mb-3.5 text-center">Back to Sign In</Link>
        </>
    );
}
