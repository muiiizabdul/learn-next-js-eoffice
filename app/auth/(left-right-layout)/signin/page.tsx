import Link from 'next/link';

export default function Page() {
    return (
        <>
            <h1>Sign In</h1>
            <p className="text-sm text-slate-500 mt-2">Enter your email and password to sign in.</p>
            <div className="my-8">
                <form>
                    <div>
                        <label className="text-base text-slate-500 font-medium text-sm">Email</label><br />
                        <input placeholder="Enter your email" type="email" className="mt-2 w-full border py-2 px-2.5 rounded font-light text-sm" />
                    </div>
                    <div className="mt-3.5">
                        <label className="text-base text-slate-500 font-medium text-sm">Password</label><br />
                        <input placeholder="Enter your password" type="password" className="mt-2 w-full border py-2 px-2.5 rounded font-light text-sm" />
                    </div>
                    <div className="mt-5">
                        <button className="w-full bg-indigo-500 py-3 text-white rounded font-medium text-sm">Sign In</button>
                        <p className="text-xs text-slate-500 mt-4 font-light">Don't have an account? <Link href="/auth/signup" className="font-medium">Sign Up</Link></p>
                        <Link href="/auth/forgot-password" className="text-xs mt-4 font-medium">Forgot Password?</Link>
                    </div>
                </form>
            </div>
        </>
    );
}
