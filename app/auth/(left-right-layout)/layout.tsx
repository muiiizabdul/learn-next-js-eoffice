import React from "react";
import Link from 'next/link';

export default function Layout({ children }: { children: React.ReactNode }) {
    return (
        <div className="flex flex-row h-screen">
            <div className="basis-1/2 bg-primary"></div>
            <div className="basis-1/2 bg-white">
                <div className="auth-form-wrapper">
                    <div className="auth-form-container">
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
}