import Link from 'next/link';

export default function Page() {
    return (
        <>
            <h1>Sign Up</h1>
            <div className="my-8">
                <form>
                    <div className=''>
                        <label className="text-base text-slate-500 font-medium text-sm">Name</label><br />
                        <input placeholder="Enter your name" type="text" className="mt-2 w-full border py-2 px-2.5 rounded font-light text-sm" />
                    </div>
                    <div className='mt-3.5'>
                        <label className="text-base text-slate-500 font-medium text-sm">Email</label><br />
                        <input placeholder="Enter your email" type="email" className="mt-2 w-full border py-2 px-2.5 rounded font-light text-sm" />
                    </div>
                    <div className="mt-3.5">
                        <label className="text-base text-slate-500 font-medium text-sm">Password</label><br />
                        <input placeholder="Enter your password" type="password" className="mt-2 w-full border py-2 px-2.5 rounded font-light text-sm" />
                    </div>
                    <div className="mt-5">
                        <button className="w-full bg-primary py-3 text-white rounded font-medium text-sm">Sign In</button>
                        <p className="text-xs text-slate-500 mt-4 font-light">Already have an account? <Link href="/auth/signin" className="font-medium">Sign In</Link></p>
                        <Link href="/auth/forgot-password" className="text-xs mt-4 font-medium">Forgot Password?</Link>
                    </div>
                </form>
            </div>
        </>
    );
}
